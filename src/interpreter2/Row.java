package interpreter2;

public class Row {
    private String name;
    private String lastName;

    public Row(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return name + " " + lastName;
    }
}
