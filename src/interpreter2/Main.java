package interpreter2;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        Expression query = new Select("*"
                , new From("people"
                , new Where(name -> name.toLowerCase().startsWith("j"))
        ));
        Context ctx = new Context();
        List<String> res =  query.interpret(ctx);
        System.out.println(res);
    }
}
