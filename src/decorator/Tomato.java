package decorator;

public class Tomato extends ToppingsDecorator {

    Pizza pizza;

    public Tomato(Pizza pizza) {
        super("Tomato Toppings");
        this.pizza = pizza;
    }

    @Override
    public int getCost() {
        return 5 + pizza.getCost();
    }

    @Override
    public String getDescription() {
        return pizza.getDescription() + " ,Tomato Topping";
    }
}