package decorator;

public class Cheese extends ToppingsDecorator {

    Pizza pizza;

    public Cheese(Pizza pizza) {
        super("Cheese Toppings");
        this.pizza = pizza;
    }

    @Override
    public int getCost() {
        return 5 + pizza.getCost();
    }

    @Override
    public String getDescription() {
        return pizza.getDescription() + " ,Cheese Topping";
    }
}
