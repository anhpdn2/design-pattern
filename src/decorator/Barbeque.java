package decorator;

public class Barbeque extends ToppingsDecorator {

    Pizza pizza;

    public Barbeque(Pizza pizza) {
        super("Barbeque Toppings");
        this.pizza = pizza;
    }

    @Override
    public int getCost() {
        return 10 + pizza.getCost();
    }

    @Override
    public String getDescription() {
        return pizza.getDescription() + " ,Barbeque Topping";
    }
}
