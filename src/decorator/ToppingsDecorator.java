package decorator;

public abstract class ToppingsDecorator extends Pizza {
    public ToppingsDecorator(String description) {
        super(description);
    }

    @Override
    public abstract String getDescription();
}
