package decorator;

public class Main {
    public static void main(String[] args) {
        Pizza pizza = new Veggie();
        System.out.println(pizza.getDescription() + " Cost: " + pizza.getCost());
        pizza = new Cheese(pizza);
        pizza = new Tomato(pizza);
        System.out.println(pizza.getDescription() + " Cost: " + pizza.getCost());
    }
}
