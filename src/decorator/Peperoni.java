package decorator;

public class Peperoni extends Pizza {

    public Peperoni() {
        super("Peperoni");
    }

    @Override
    public int getCost() {
        return 30;
    }
}
