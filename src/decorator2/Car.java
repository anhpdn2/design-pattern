package decorator2;

public interface Car {
    String assemble();
}
