package decorator2;

public class Main {
    public static void main(String[] args) {
        Car car = new LuxuryCar(new SportCar(new CarBasic()));
        System.out.println(car.assemble());
    }
}
