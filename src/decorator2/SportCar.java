package decorator2;

public class SportCar extends CarDecorator {
    Car car;

    public SportCar(Car car) {
        this.car = car;
    }

    @Override
    public String assemble() {
        return car.assemble() + "Sport, ";
    }
}
