package decorator2;

public class LuxuryCar extends CarDecorator {
    Car car;

    public LuxuryCar(Car car) {
        this.car = car;
    }

    @Override
    public String assemble() {
        return car.assemble() + "Luxury.";
    }
}
