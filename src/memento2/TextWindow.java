package memento2;

public class TextWindow {
    private StringBuilder currentText;

    public TextWindow() {
        this.currentText = new StringBuilder();
    }

    public void addText(String text) {
        currentText.append(text);
    }

    // save the momento
    public TextWindowMemento save() {
        return new TextWindowMemento(currentText.toString());
    }

    public void restore(TextWindowMemento save) {
        currentText = new StringBuilder(save.getText());
    }

    public String getCurrentText() {
        return currentText.toString();
    }
}
