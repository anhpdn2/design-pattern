package memento2;

public class TextWindowMemento {
    private String text;

    public TextWindowMemento(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }


}
