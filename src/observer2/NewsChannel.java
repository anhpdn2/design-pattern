package observer2;

public class NewsChannel implements Channel {
    public String news;
    NewsAgency newsAgency;

    public NewsChannel(NewsAgency newsAgency) {
        this.newsAgency = newsAgency;
        newsAgency.addObserver(this);
    }

    @Override
    public void update(Object o) {
        System.out.println("News updated! " + newsAgency.getNews());
    }
}
