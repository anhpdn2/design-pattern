package observer2;

public interface Channel {
    void update(Object o);
}
