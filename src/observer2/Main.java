package observer2;

public class Main {
    public static void main(String[] args) {
        NewsAgency observable = new NewsAgency();
        NewsChannel observer = new NewsChannel(observable);
        NewsChannel abcNews = new NewsChannel(observable);
        observable.setNews("Breaking News");
    }
}
