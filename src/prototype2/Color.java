package prototype2;

public interface Color {
    void applyColor();
    Color clone();
}
