package prototype2;

import java.util.HashMap;
import java.util.Map;

public class ColorStore {
    Map<String, Color> colorMap = new HashMap<>();
    public void duplicate(Color color) {
        Color newColor = color.clone();
        colorMap.put("Red", newColor);
    }
}
