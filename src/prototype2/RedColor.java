package prototype2;

public class RedColor implements Color {
    private String colorName = "Red";

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    @Override
    public void applyColor() {
        System.out.println("Applying " + colorName + " color");
    }

    @Override
    public Color clone() {
        RedColor c = new RedColor();
        c.applyColor();
        c.setColorName("RED");
        return c;
    }
}
