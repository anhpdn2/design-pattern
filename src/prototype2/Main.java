package prototype2;

public class Main {
    public static void main(String[] args) {
        ColorStore colorStore = new ColorStore();
        colorStore.duplicate(new RedColor());
        System.out.println(colorStore.colorMap.size());
    }
}
