package visitor2;

public interface ComputerPart {
    void accept(ComputerPartVisitor computerPartVisitor);
}
