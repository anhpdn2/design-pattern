package visitor2;

public class Main {
    public static void main(String[] args) {
        ComputerPart computerPart = new Keyboard();
        computerPart.accept(new ComputerPartDisplayVisitor());
    }
}
