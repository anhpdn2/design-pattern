package composite2;

public class Main {
    public static void main(String[] args) {
        File[] f = new File[20];

        f[0] = new Directory("Root");
        f[1] = new Directory("My Documents");
        f[2] = new Directory("Maths");
        f[3] = new AFile("primary-numbers.txt");
        f[4] = new AFile("evens-numbers.txt");

        ((Directory)f[0]).addFile(f[1]);
        ((Directory)f[1]).addFile(f[2]);
        ((Directory)f[2]).addFile(f[3]);
        ((Directory)f[2]).addFile(f[4]);

        System.out.println(f[0].display());
    }
}
