package composite2;

public class AFile extends File {
    public AFile(String fileName) {
        super(fileName);
    }

    @Override
    String display() {
        return getFileName();
    }
}
