package composite2;

public class FileAttributey implements Comparable<FileAttributey>{
    Attributey attributey;
    String value;

    public FileAttributey(Attributey attributey, String value) {
        this.attributey = attributey;
        this.value = value;
    }

    @Override
    public int compareTo(FileAttributey o) {
        return attributey.attributeName.compareTo(o.attributey.attributeName);
    }
}
