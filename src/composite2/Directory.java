package composite2;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

public class Directory extends File {
    Set<File> files;

    public Directory(String fileName) {
        super(fileName);
        files = new TreeSet<>();
    }

    public boolean addFile(File file) {
        return files.add(file);
    }

    public boolean removeFile(File file) {
        return files.remove(file);

    }

    public String[] list() {
        return (String[]) files.stream().map(File::getFileName).toArray();
    }

    public File[] listFiles() {
        return (File[]) files.toArray();
    }

    @Override
    String display() {
        StringBuffer out = new StringBuffer(this.fileName + "\n");
        files.forEach(f -> {
            if (f instanceof AFile) {
                out.append(f.fileName + ",");
            }
            if (f instanceof Directory) {
                out.append(f.display());
            }
        });
        return out.toString();
    }
}
