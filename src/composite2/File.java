package composite2;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public abstract class File implements Comparable<File> {
    String fileName;
    Set<FileAttributey> fileAttributes;

    public File(String fileName) {
        this.fileName = fileName;
        this.fileAttributes = new TreeSet<>();
    }

    public FileAttributey getFileAttributey(Attributey attribute) {
        if (!fileAttributes.isEmpty()) {
            return fileAttributes.stream().filter(f -> f.equals(attribute)).findFirst().get();
        }
        return null;
    }

    public String getFileName() {
        return fileName;
    }



    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Set<FileAttributey> getFileAttributes() {
        return fileAttributes;
    }

    public void removeFileAttributes(FileAttributey attribute) {
        fileAttributes.remove(attribute);
    }

    public void addFileAttributes(FileAttributey attribute) {
        fileAttributes.add(attribute);
    }

    @Override
    public int compareTo(File o) {
        return fileName.compareTo(o.getFileName());
    }

    abstract String display();
}
