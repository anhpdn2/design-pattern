package abstractfactory;

public class RoundedSquare implements Shape {
    @Override
    public void drawShape() {
        System.out.println("Drawing RoundedSquare");
    }
}
