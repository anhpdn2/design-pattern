package abstractfactory;

public class RoundedRectangle implements Shape {
    @Override
    public void drawShape() {
        System.out.println("Drawing RoundedRectangle");
    }
}
