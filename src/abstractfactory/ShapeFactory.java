package abstractfactory;

public class ShapeFactory implements AbstractFactory{
    public Shape getShape(String typeShape) {
        if (typeShape.equalsIgnoreCase("Circle")) {
            return new Circle();
        } else if (typeShape.equalsIgnoreCase("Rectangle")) {
            return new Rectangle();
        } else if (typeShape.equalsIgnoreCase("Square")) {
            return new Square();
        }
        return null;
    }
}
