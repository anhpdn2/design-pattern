package abstractfactory;

public interface Shape {
    void drawShape();
}
