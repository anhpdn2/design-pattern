package abstractfactory;

public class Square implements Shape {
    @Override
    public void drawShape() {
        System.out.println("Drawing Square");
    }
}
