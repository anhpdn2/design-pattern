package abstractfactory;

public class Main {
    public static void main(String[] args) {
        ProducerFactory pf = new ProducerFactory();
        AbstractFactory af = pf.getTypeFactory(true);
        Shape s = af.getShape("Rectangle");
        s.drawShape();
    }
}
