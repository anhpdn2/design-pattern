package abstractfactory;

public interface AbstractFactory {
    Shape getShape(String shape);
}
