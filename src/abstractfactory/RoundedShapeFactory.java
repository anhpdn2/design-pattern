package abstractfactory;

public class RoundedShapeFactory  implements AbstractFactory {
    public Shape getShape(String typeShape) {
        if (typeShape.equalsIgnoreCase("Rectangle")) {
            return new RoundedRectangle();
        } else if (typeShape.equalsIgnoreCase("Square")) {
            return new RoundedSquare();
        }
        return null;
    }
}
