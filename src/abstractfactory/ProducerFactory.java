package abstractfactory;

public class ProducerFactory {
    public AbstractFactory getTypeFactory(boolean rounded) {
        if (rounded) {
            return new RoundedShapeFactory();
        }
        return new ShapeFactory();
    }
}
