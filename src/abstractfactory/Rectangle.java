package abstractfactory;

public class Rectangle implements Shape {
    @Override
    public void drawShape() {
        System.out.println("Drawing Rectangle");
    }
}
