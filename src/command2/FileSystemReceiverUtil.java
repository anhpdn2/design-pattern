package command2;

public class FileSystemReceiverUtil {
    public static FileSystemReceiver getUnderlyingFileSystem() {
        String osName = System.getProperty("os.name");
        System.out.println("Current OS is: " + osName);
        return osName.contains("Windows") ? new Windows() : new Linux();
    }
}
