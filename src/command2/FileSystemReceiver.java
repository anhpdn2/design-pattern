package command2;

public interface FileSystemReceiver {
    void openFile();
    void saveFile();
    void closeFile();
}
