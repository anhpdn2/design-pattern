package command2;

public class Main {
    public static void main(String[] args) {
        FileSystemReceiver fs = FileSystemReceiverUtil.getUnderlyingFileSystem();


        // set Command
        OpenFileCommand of = new OpenFileCommand(fs);
        SaveFileCommand sf = new SaveFileCommand(fs);

        // call file util
        FileInvoker fileInvoker = new FileInvoker(of);
        FileInvoker fileInvoker2 = new FileInvoker(sf);

        // execute
        fileInvoker.execute();
        fileInvoker2.execute();
    }
}
