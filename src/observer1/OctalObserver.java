package observer1;

public class OctalObserver extends Observery {
    public OctalObserver(Subject subject) {
        this.subject = subject;
        subject.subscribeToList(this);
    }

    @Override
    public void update() {
        System.out.println("Octal String: " + Integer.toBinaryString(subject.getState()));;
    }
}
