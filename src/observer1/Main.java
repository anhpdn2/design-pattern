package observer1;

public class Main {
    public static void main(String[] args) {
        Subject s = new Subject();
        new BinaryObserver(s);
        new OctalObserver(s);
        new HexaObserver(s);
        s.setState(20);
        s.setState(30);
    }
}
