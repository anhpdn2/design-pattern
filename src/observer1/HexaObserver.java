package observer1;

public class HexaObserver extends Observery {
    public HexaObserver(Subject subject) {
        this.subject = subject;
        subject.subscribeToList(this);
    }

    @Override
    public void update() {
        System.out.println("Hexa String: " + Integer.toBinaryString(subject.getState()));;
    }
}
