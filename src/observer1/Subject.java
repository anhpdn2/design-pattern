package observer1;

import java.util.ArrayList;
import java.util.List;

public class Subject {
    private List<Observery> observers = new ArrayList<>();

    int state;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
        notifyAllObserver();
    }

    public void subscribeToList(Observery observery) {
        observers.add(observery);
    }

    private void notifyAllObserver() {
        for (Observery ob : observers) {
            ob.update();
        }
    }
}
