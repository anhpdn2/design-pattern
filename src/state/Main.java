package state;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        PullingFan pf = new PullingFan();
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.println("Press Enter");
            String k = sc.nextLine();
            if (k.isEmpty()) {
                pf.push();
            }
        }
    }
}
