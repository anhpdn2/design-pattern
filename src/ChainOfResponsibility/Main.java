package ChainOfResponsibility;

public class Main {
    public static void main(String[] args) {
        Chain chain1 = new AddNumbers();
        Chain chain2 = new SubtractNumbers();
        Chain chain3 = new MultipleNumbers();
        Chain chain4 = new DivineNumbers();
        chain1.setNextChain(chain2);
        chain2.setNextChain(chain3);
        chain3.setNextChain(chain4);
        Numbers numbers = new Numbers(1, 3, "sub");
        chain1.calculate(numbers);
    }
}
