package adapter1;

public class Masserati extends Vehicle {
    public Masserati() {
        super("adapter1.Masserati");
    }

    @Override
    int getSpeed() {
        return 220;
    }
}
