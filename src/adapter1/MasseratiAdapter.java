package adapter1;

public class MasseratiAdapter extends VehicleAdapter {

    public MasseratiAdapter() {
        super("adapter1.Masserati");
    }

    @Override
    public int speedKMH() {
        return (int) (220 * 1.6);
    }
}
