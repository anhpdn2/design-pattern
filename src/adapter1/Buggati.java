package adapter1;

public class Buggati extends Vehicle {
    public Buggati() {
        super("adapter1.Buggati");
    }

    @Override
    int getSpeed() {
        return 240;
    }
}
