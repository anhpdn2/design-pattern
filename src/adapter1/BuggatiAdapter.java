package adapter1;

public class BuggatiAdapter extends VehicleAdapter {

    public BuggatiAdapter() {
        super("adapter1.Buggati");
    }

    @Override
    public int speedKMH() {
        return (int)(240 * 1.6);
    }
}
