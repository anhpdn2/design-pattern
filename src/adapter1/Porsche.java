package adapter1;

public class Porsche extends Vehicle {
    public Porsche() {
        super("adapter1.Porsche");
    }

    @Override
    int getSpeed() {
        return 200;
    }
}
