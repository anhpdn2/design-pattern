package adapter1;

public abstract class Vehicle {
    protected String brand;

    public Vehicle(String brand) {
        this.brand = brand;
    }

    abstract int getSpeed();

    @Override
    public String toString() {
        return brand + " : " + getSpeed() + " MPH";
    }
}
