package adapter1;

public class PorscheAdapter extends VehicleAdapter {


    public PorscheAdapter() {
        super("adapter1.Porsche");
    }

    @Override
    public int speedKMH() {
        return (int) (200 * 1.6);
    }
}
