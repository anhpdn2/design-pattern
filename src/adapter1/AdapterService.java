package adapter1;

// Adaptee or Service
public interface AdapterService {

    int speedKMH();
    String toString();

}
