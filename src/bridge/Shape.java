package bridge;

public abstract class Shape {

    Color color;

    public Shape(Color color) {
        this.color = color;
    }

    public abstract void applyColorToShape();

    @Override
    public String toString() {
        return "The shape is: " + color + " ";
    }
}
