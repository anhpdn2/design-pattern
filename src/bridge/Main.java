package bridge;

public class Main {
    public static void main(String[] args) {
        Shape s1 = new Circle(new RedColor());
        s1.applyColorToShape();
    }
}
