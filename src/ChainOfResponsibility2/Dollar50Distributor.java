package ChainOfResponsibility2;

public class Dollar50Distributor implements MoneyDistributor {
    private MoneyDistributor moneyDistributor;

    @Override
    public void setNextChain(MoneyDistributor distributor) {
        moneyDistributor = distributor;
    }

    @Override
    public void distribute(Currency currency) {
        if (currency.getAmount() >= 50) {
            int num = currency.getAmount() / 50;
            int remainder = currency.getAmount() % 50;
            System.out.println("Distributing " + num + " 50$ note");
            if (remainder != 0) {
                this.moneyDistributor.distribute(new Currency(remainder));
            }
        } else {
            this.moneyDistributor.distribute(currency);
        }
    }
}
