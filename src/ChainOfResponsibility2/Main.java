package ChainOfResponsibility2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Dollar10Distributor d10 = new Dollar10Distributor();
        Dollar20Distributor d20 = new Dollar20Distributor();
        Dollar50Distributor d50 = new Dollar50Distributor();
        d50.setNextChain(d20);
        d20.setNextChain(d10);
        int amount = 0;
        System.out.println("Enter amount to withdraw: ");
        Scanner sc = new Scanner(System.in);
        amount = sc.nextInt();
        if(amount % 10 != 0) {
            System.out.println("Invalid number % 10");
            return;
        }
        d50.distribute(new Currency(amount));

    }
}
