package ChainOfResponsibility2;

public class Dollar10Distributor implements MoneyDistributor {
    private MoneyDistributor moneyDistributor;

    @Override
    public void setNextChain(MoneyDistributor distributor) {
        moneyDistributor = distributor;
    }

    @Override
    public void distribute(Currency currency) {
        if (currency.getAmount() >= 10) {
            int num = currency.getAmount() / 10;
            int remainder = currency.getAmount() % 10;
            System.out.println("Distributing " + num + " 10$ note");
            if (remainder != 0) {
                this.moneyDistributor.distribute(new Currency(remainder));
            }
        } else {
            System.out.println("No bills less than 10$");
        }
    }
}
