package ChainOfResponsibility2;

public interface MoneyDistributor {
    void setNextChain(MoneyDistributor distributor);
    void distribute(Currency currency);
}
