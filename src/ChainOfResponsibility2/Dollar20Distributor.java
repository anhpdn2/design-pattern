package ChainOfResponsibility2;

public class Dollar20Distributor implements MoneyDistributor {
    private MoneyDistributor moneyDistributor;

    @Override
    public void setNextChain(MoneyDistributor distributor) {
        moneyDistributor = distributor;
    }

    @Override
    public void distribute(Currency currency) {
        if (currency.getAmount() >= 20) {
            int num = currency.getAmount() / 20;
            int remainder = currency.getAmount() % 20;
            System.out.println("Distributing " + num + " 20$ note");
            if (remainder != 0) {
                this.moneyDistributor.distribute(new Currency(remainder));
            }
        } else {
            this.moneyDistributor.distribute(currency);
        }
    }
}
