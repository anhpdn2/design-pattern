package visitor;

public interface Pizza {
    String order();
}
