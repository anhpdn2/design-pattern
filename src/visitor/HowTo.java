package visitor;

public interface HowTo {
    void visit(Pizza pizza);
}
