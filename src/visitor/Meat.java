package visitor;

public class Meat implements Pizza{
    @Override
    public String order() {
        return "Meat Pizza";
    }
}
