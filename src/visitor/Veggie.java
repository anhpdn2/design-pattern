package visitor;

public class Veggie implements Pizza{
    @Override
    public String order() {
        return "Veggie Pizza";
    }
}
