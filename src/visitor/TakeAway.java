package visitor;

public class TakeAway implements HowTo {
    @Override
    public void visit(Pizza pizza) {
        pizza.order();
        System.out.println("TakeAway" + pizza.toString());
    }
}
