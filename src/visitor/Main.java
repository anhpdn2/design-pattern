package visitor;

public class Main {
    public static void main(String[] args) {
        Pizza pizza = new Veggie();
        HowTo deliveryMe = new TakeAway();
        deliveryMe.visit(pizza);
    }
}
