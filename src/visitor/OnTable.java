package visitor;

public class OnTable implements HowTo {
    @Override
    public void visit(Pizza pizza) {
        pizza.order();
        System.out.println("Ontable" + pizza.toString());
    }
}
