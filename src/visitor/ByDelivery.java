package visitor;

public class ByDelivery implements HowTo {
    @Override
    public void visit(Pizza pizza) {
        pizza.order();
        System.out.println("ByDelivery" + pizza.toString());
    }
}
