package factory;

public interface Shape {
    void drawShape();
}
