package factory;

public class Main {
    public static void main(String[] args) {
        ShapeFactory sf = new ShapeFactory();
        Shape s1 = sf.getShape("Circle");
        s1.drawShape();
    }
}
