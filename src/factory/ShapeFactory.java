package factory;

public class ShapeFactory {
    public Shape getShape(String shape) {
        if (shape.equalsIgnoreCase("circle")) {
            return new Circle();
        } else {
            return new Triangle();
        }
    }
}
