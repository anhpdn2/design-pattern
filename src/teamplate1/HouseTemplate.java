package teamplate1;

public abstract class HouseTemplate {

    // Template Method
    // final
    public final void buildHouse() {
        buildFoundation();
        buildPillars();
        buildWalls();
        buildWindows();
        System.out.println("House is Built!");
    }

    private void buildWindows() {
        System.out.println("Building Glass Windows");
    }

    private void buildFoundation() {
        System.out.println("Building Foundation (cement, Iron rods & Sand");
    }

    public abstract void buildWalls();

    public abstract void buildPillars();


}
