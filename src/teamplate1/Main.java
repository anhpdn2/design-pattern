package teamplate1;

public class Main {
    public static void main(String[] args) {
        HouseTemplate woodenh = new WoodenHouse();
        woodenh.buildHouse();
        HouseTemplate glass = new GlassHouse();
        glass.buildHouse();
    }
}
