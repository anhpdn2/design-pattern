package singleton;

public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {
        SingletonConn s1 = SingletonConn.getOnlyInstance();
        System.out.println(s1);
        SingletonConn s2 = SingletonConn.getOnlyInstance();
        System.out.println(s2);
        // luu y clone

    }
}
