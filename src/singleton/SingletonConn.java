package singleton;

public class SingletonConn implements Cloneable {
    private SingletonConn() {}

    private static SingletonConn onlyInstance;

    public static SingletonConn getOnlyInstance() {
        if (onlyInstance == null) {
            onlyInstance = new SingletonConn();
        }
        return onlyInstance;
    }

    @Override
    protected Object clone() {
        return new CloneNotSupportedException("sai");
    }
}
