package factory2;

public class Email implements Notification {
    @Override
    public void notifyUser() {
        System.out.println("Send Email");
    }
}
