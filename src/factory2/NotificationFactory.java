package factory2;

public class NotificationFactory {
    public static Notification sendNoti(String name) {
        switch (name) {
            case "SMS":
                return new SMS();
            case "EMAIL":
                return new Email();
            case "PUSH":
                return new PushNoty();
            default:
                throw new IllegalArgumentException("ngu");
        }
    }
}
