package factory2;

public interface Notification {
    void notifyUser();
}
