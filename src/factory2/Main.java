package factory2;

public class Main {
    public static void main(String[] args) {
        Notification noti = NotificationFactory.sendNoti("SMS");
        noti.notifyUser();
    }
}
