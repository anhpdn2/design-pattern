package adapter2;

public class TriangleAdapter extends Triangle implements Shape {

    public TriangleAdapter(double a, double b, double c) {
        super(a, b, c);
    }

    @Override
    public void draw() {
        drawShape();
    }

    @Override
    public void resize() {
        System.out.println(description() + " can't be resize");
    }

    @Override
    public String description() {
        return "Triangle Object";
    }

    @Override
    public boolean isHide() {
        return false;
    }
}
