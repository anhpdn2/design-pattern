package adapter2;

public class RhombusAdapter extends Rhombus implements Shape {
    public RhombusAdapter(double a, double b) {
        super(a, b);
    }

    @Override
    public void draw() {
        drawShape();
    }

    @Override
    public void resize() {
        System.out.println(description() + " can't be resize");
    }

    @Override
    public String description() {
        return "Rhombus Object";
    }

    @Override
    public boolean isHide() {
        return false;
    }
}
