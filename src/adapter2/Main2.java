package adapter2;

public class Main2 {
    public static void main(String[] args) {
        Rhombus r = new Rhombus(4,5);
        r.drawShape();
        Shape t = new  GeometricShapeObjectAdapter(new Triangle(3,4,5));
        t.draw();
        Shape t2 = new TriangleAdapter(3,4,5);
        t2.draw();
    }
}
