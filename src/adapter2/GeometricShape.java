package adapter2;

public interface GeometricShape {
    double area();
    double perimeter();
    void drawShape();

}
