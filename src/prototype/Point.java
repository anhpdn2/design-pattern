package prototype;

public class Point extends Shape{
    private int x;
    private int y;

    public Point(int x, int y) {
        super(x, y);
    }

    @Override
    public Shape clone() {
        return new Point(x, y);
    }
}
