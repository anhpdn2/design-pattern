package prototype;

public class Main {
    public static void main(String[] args) {
        Shape s1 = new Point(1,3);
        Shape s2 =  s1.clone();
        Shape s3 =  s2.clone();
        System.out.println(s2.getTx());
        System.out.println(s3.getTx());
    }
}
