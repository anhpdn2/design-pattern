package interpreter;

public class Main {
    public static void main(String[] args) {
        Expression isMale = getMaleExpression();
        Expression isMarriedWoman = getMarriedWomanExpression();
        System.out.println("Jack is male? " + isMale.interpret("Jack"));
        System.out.println("Sara is a married woman? " + isMarriedWoman.interpret("Sara Married"));
    }
    public static Expression getMaleExpression() {
        Expression jack = new TerminalExpression("Jack");
        Expression john = new TerminalExpression("John");
        return new OrExpression(jack, john);
    }
    public static Expression getMarriedWomanExpression() {
        Expression sara = new TerminalExpression("Sara");
        Expression married = new TerminalExpression("Married");
        return new AndExpression(sara, married);
    }
}
