package template2;

public abstract class ComputerTemplate {
    public final void buildComputer() {
        buildMotherBoard();
        buildCPU();
        buildHDD();
        buildGraphics();
    }

    public abstract void buildMotherBoard();
    public abstract void buildCPU();
    public abstract void buildHDD();
    public abstract void buildGraphics();
}
