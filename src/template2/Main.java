package template2;

public class Main {
    public static void main(String[] args) {
        ComputerTemplate st = new StandardComputer();
        st.buildComputer();
        ComputerTemplate sc = new SuperComputer();
        sc.buildComputer();

    }
}
