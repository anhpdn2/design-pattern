package template2;

public class SuperComputer extends ComputerTemplate {
    @Override
    public void buildMotherBoard() {
        System.out.println("TUF Mother Board");
    }

    @Override
    public void buildCPU() {
        System.out.println("i9 12600f");

    }

    @Override
    public void buildHDD() {
        System.out.println("2tb nvme");

    }

    @Override
    public void buildGraphics() {
        System.out.println("4070TI");

    }
}
