package template2;

public class StandardComputer extends ComputerTemplate {
    @Override
    public void buildMotherBoard() {

        System.out.println("b610M");
    }

    @Override
    public void buildCPU() {

        System.out.println("i3 12600f");
    }

    @Override
    public void buildHDD() {

        System.out.println("1TB HDD samsung");
    }

    @Override
    public void buildGraphics() {

        System.out.println("1660 super");
    }
}
