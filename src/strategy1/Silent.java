package strategy1;

public class Silent implements MobileState {
    @Override
    public void alert(AlertStateContext ctx) {
        System.out.println("Silent mode");
    }
}
