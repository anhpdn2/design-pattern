package strategy1;

public interface MobileState {
    void alert(AlertStateContext ctx);
}
