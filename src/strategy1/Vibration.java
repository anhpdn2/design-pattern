package strategy1;

public class Vibration implements MobileState {
    @Override
    public void alert(AlertStateContext ctx) {
        System.out.println("Vibration mode");
    }
}
