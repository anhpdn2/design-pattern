package strategy1;

public class Main {
    public static void main(String[] args) {
        AlertStateContext ctx = new AlertStateContext();
        ctx.alert();

        ctx.setCurrentState(new Silent());
        ctx.alert();
    }
}
