package state2;


import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ButtonRinging pf = new ButtonRinging(new Ringing());
        Scanner sc = new Scanner(System.in);

        while (true) {
            System.out.println("Press Enter");
            String k = sc.nextLine();
            if (k.isEmpty()) {
                pf.pushButtonState();
            }
        }
    }
}
