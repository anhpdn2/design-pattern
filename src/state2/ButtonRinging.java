package state2;

public class ButtonRinging {
    private ButtonState buttonState;

    public ButtonRinging(ButtonState buttonState) {
        this.buttonState = buttonState;
    }

    public void pushButtonState() {
        buttonState.switchState(this);
    }

    public void setButtonState(ButtonState buttonState) {
        this.buttonState = buttonState;
    }
}
