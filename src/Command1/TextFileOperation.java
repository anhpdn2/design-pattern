package Command1;

public interface TextFileOperation {
    String execute();
}
