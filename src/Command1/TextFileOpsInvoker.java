package Command1;

import java.util.ArrayList;
import java.util.List;

public class TextFileOpsInvoker {
    private final List<TextFileOperation> operationList = new ArrayList<>();

    public String executeOperation(TextFileOperation operation) {
        operationList.add(operation);
        return operation.execute();
    }
}
