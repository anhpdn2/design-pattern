package Command1;

public class Main {
    public static void main(String[] args) {
        TextFileOpsInvoker invoker = new TextFileOpsInvoker();
        TextFile textFile = new TextFile("file1.txt");


        System.out.println(invoker.executeOperation(new OpenTextFileOps(textFile)));
        System.out.println(invoker.executeOperation(new SaveTextFileOps(textFile)));
    }
}
