package Memento;

public class Originator {
    private String state;

    public Originator(String state) {
        this.state = state;
    }

    public Originator() {
    }

    public Memento saveStateToMemento() {
        return new Memento(state);
    }

    // Restoring memento
    public void getStateFromMemento(Memento memento) {
        state = memento.getState();
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }
}
