package Memento;

public class Main {
    public static void main(String[] args) {
        Originator originator = new Originator();
        CareTaker careTaker = new CareTaker();
        originator.setState("ducanh1");
        careTaker.add(originator.saveStateToMemento());
        originator.setState("ducanh2");
        careTaker.add(originator.saveStateToMemento());
        System.out.println(originator.getState());
        originator.getStateFromMemento(careTaker.get(0));
        System.out.println(originator.getState());

    }
}
