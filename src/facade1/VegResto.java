package facade1;

public class VegResto implements Hotel{
    @Override
    public Menus getMenus() {
        VegMenu vegMenu = new VegMenu();
        return vegMenu;
    }
}
