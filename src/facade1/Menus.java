package facade1;

public abstract class Menus {
    private String menu;

    public String getMenu() {
        return menu;
    }

    public Menus(String menu) {
        this.menu = menu;
    }
}
