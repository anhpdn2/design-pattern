package facade1;

public class HotelKeeper {
    public VegMenu getVegMenu() {
        VegResto v = new VegResto();
        VegMenu vegMenu = (VegMenu)v.getMenus();
        return vegMenu;
    }

    public NonVegMenu getNonVegMenu() {
        NonVegResto v = new NonVegResto();
        NonVegMenu nvegMenu = (NonVegMenu)v.getMenus();
        return nvegMenu;
    }

    public BothVegNon getVegNonVegMenu() {
        BothRestro b = new BothRestro();
        BothVegNon bothMenus = (BothVegNon)b.getMenus();
        return bothMenus;
    }
}
