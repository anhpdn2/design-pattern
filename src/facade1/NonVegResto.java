package facade1;

public class NonVegResto implements Hotel {
    @Override
    public Menus getMenus() {
        NonVegMenu nv = new NonVegMenu();
        return nv;
    }
}
