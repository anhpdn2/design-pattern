package facade1;

public class Main {
    public static void main(String[] args) {
        HotelKeeper keeper = new HotelKeeper();
        VegMenu v = keeper.getVegMenu();
        System.out.println(v.getMenu());
        BothVegNon v1 = keeper.getVegNonVegMenu();
        System.out.println(v1.getMenu());
    }
}
