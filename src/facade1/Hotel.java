package facade1;

public interface Hotel {
    public Menus getMenus();
}
