package composite1;

import java.util.ArrayList;
import java.util.List;

public class Circle implements Shape {
    private String initColor;
    public Circle(String color) {
        this.initColor = color;
        System.out.println("Creating Circle with color: " + initColor);
    }

    @Override
    public void draw(String fillColor) {
        System.out.println("Drawing Circle with color: " + fillColor);
    }
}
