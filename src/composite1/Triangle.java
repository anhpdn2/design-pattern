package composite1;

public class Triangle implements Shape {
    private String initColor;
    public Triangle(String color) {
        this.initColor = color;
        System.out.println("Creating Triangle with color: " + initColor);
    }

    @Override
    public void draw(String fillColor) {
        System.out.println("Drawing Triangle with color: " + fillColor);
    }
}
