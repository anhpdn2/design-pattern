package composite1;

public interface Shape {
    void draw(String fillColor);
}
