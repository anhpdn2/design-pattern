package bridge2;

public class Main {
    public static void main(String[] args) {
        Vehicle v1 = new Truck(new Produce(), new Assemble());
        v1.manufacture();
        Vehicle v2 = new Car(new Produce(), new Assemble());
        v2.manufacture();
    }
}
