package bridge2;

public class Car extends Vehicle {

    public Car(Workshop produreWorkshop, Workshop assembleWorkshop) {
        super(produreWorkshop, assembleWorkshop);
    }

    @Override
    public void manufacture() {
        System.out.print("Car ");
        produreWorkshop.work();
        assembleWorkshop.work();
    }
}
