package bridge2;

public class Truck extends Vehicle {

    public Truck(Workshop produreWorkshop, Workshop assembleWorkshop) {
        super(produreWorkshop, assembleWorkshop);
    }

    @Override
    public void manufacture() {
        System.out.print("Truck ");
        produreWorkshop.work();
        assembleWorkshop.work();
    }
}
