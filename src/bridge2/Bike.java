package bridge2;

public class Bike extends Vehicle {

    public Bike(Workshop assembleWorkshop, Workshop produreWorkshop) {
        super(assembleWorkshop, produreWorkshop);
    }

    @Override
    public void manufacture() {
        System.out.print("Bike ");
        assembleWorkshop.work();
        produreWorkshop.work();
    }
}
