package bridge2;

public abstract class Vehicle {
    Workshop assembleWorkshop;
    Workshop produreWorkshop;

    public Vehicle(Workshop produreWorkshop, Workshop assembleWorkshop) {
        this.assembleWorkshop = assembleWorkshop;
        this.produreWorkshop = produreWorkshop;
    }

    abstract public void manufacture();
}
