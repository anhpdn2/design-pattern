package bridge2;

public class Assemble implements Workshop {
    @Override
    public void work() {
        System.out.print(" And Assemble");
    }
}
