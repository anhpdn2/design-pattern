package builder;

public abstract class Meat extends Ingredient {
    public Meat(String name) {
        super(name);
    }
}
