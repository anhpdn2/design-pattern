package builder;

public class Main {
    public static void main(String[] args) {
        Hamburger h = new Hamburger();
        h.createSandwich();
    }
}
