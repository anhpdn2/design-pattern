package mediator2;

public class Mediator {
    private Button button;
    private Fan fan;
    private PowerSupplier powerSupplier;

    public Mediator(Button button, Fan fan, PowerSupplier powerSupplier) {
        this.button = button;
        this.fan = fan;
        this.powerSupplier = powerSupplier;
    }

    public Mediator() {
    }

    public void press() {
        if(fan.isOn()) {
            fan.turnOff();
        } else {
            fan.turnOn();
        }
    }
    public void start() {
        powerSupplier.turnOn();
    }
    public void stop() {
        powerSupplier.turnOff();
    }

    public void setButton(Button button) {
        this.button = button;
        this.button.setMediator(this);
    }

    public void setFan(Fan fan) {
        this.fan = fan;
        this.fan.setMediator(this);
    }

    public void setPowerSupplier(PowerSupplier powerSupplier) {
        this.powerSupplier = powerSupplier;
    }
}
