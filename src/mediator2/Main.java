package mediator2;

public class Main {
    public static void main(String[] args) {
        Mediator mediator = new Mediator();
        Fan fan = new Fan(mediator, true);
        Button btn = new Button(mediator);
        PowerSupplier ps = new PowerSupplier();
        mediator.setButton(btn);
        mediator.setPowerSupplier(ps);
        mediator.setFan(fan);
        mediator.press();
    }
}
