package mediator2;

public class PowerSupplier {
    public void turnOn() {
        System.out.println("Power Supplier On");
    }

    public void turnOff() {
        System.out.println("Power Supplier Off");
    }
}
