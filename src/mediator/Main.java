package mediator;

public class Main {
    public static void main(String[] args) {
        ChatMediator chatMediator = new ChatMediatorConcrete();
        User u1 = new UserConcrete(chatMediator, "ducanh");
        User u2 = new UserConcrete(chatMediator, "m");
        chatMediator.addUser(u1);
        chatMediator.addUser(u2);
        u1.send("chao cac ban");

    }
}
