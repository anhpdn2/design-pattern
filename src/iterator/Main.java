package iterator;

public class Main {
    public static void main(String[] args) {
        MyDataStructure own = new MyDataStructure();
        for(Iterator o = own.getIterator(); o.hasNext();) {
            System.out.println("Employee: " + o.next());
        }
    }
}
