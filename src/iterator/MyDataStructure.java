package iterator;

public class MyDataStructure implements Container {
    public String employees[] = {"Jack", "Sara","Smith"};
    @Override
    public Iterator getIterator() {
        return new ElementIterator();
    }

    private class ElementIterator implements Iterator {
        int index;
        @Override
        public boolean hasNext() {
            return index < employees.length;
        }

        @Override
        public Object next() {
            if (index < employees.length) {
                return employees[index++];
            } else {
                return null;
            }
        }
    }

}
