package strategy2;

public  interface SocialStrategy {
    void connectTo(String friendName);
}
