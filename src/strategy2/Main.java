package strategy2;

public class Main {
    public static void main(String[] args) {
        SocialMediaContext ctx = new SocialMediaContext();
        ctx.setSocialStrategy(new Facebook());
        ctx.connectTo("DucANh");
    }
}
