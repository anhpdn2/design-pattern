package strategy2;

public class SocialMediaContext {
    private SocialStrategy socialStrategy;

    public void setSocialStrategy(SocialStrategy socialStrategy) {
        this.socialStrategy = socialStrategy;
    }

    public void connectTo(String name) {
        socialStrategy.connectTo(name);
    }
}
