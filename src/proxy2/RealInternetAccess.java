package proxy2;

public class RealInternetAccess implements OfficeInternetAccess {

    private int employeeRank;

    public RealInternetAccess(int employeeRank) {
        this.employeeRank = employeeRank;
    }

    @Override
    public void grantInternetAccess() {
        System.out.println("Internet access granted! - Employee Rank " + employeeRank);
    }
}
