package proxy2;

public interface OfficeInternetAccess {
    void grantInternetAccess();
}
