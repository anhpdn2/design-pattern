package proxy2;

public class ProxyNetAccess implements OfficeInternetAccess {
    private int employeeRank;
    private RealInternetAccess realInternetAccess;

    public ProxyNetAccess(int employeeRank) {
        this.employeeRank = employeeRank;
    }

    @Override
    public void grantInternetAccess() {
        if (employeeRank >= 5) {
            realInternetAccess = new RealInternetAccess(employeeRank);
            realInternetAccess.grantInternetAccess();
        } else {
            System.out.println("Internet Access Denied, Job level < 5");
        }
    }
}
